<?php

	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST'); 
	header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
	header('Content-type: application/json');

	$requestSUA = json_decode(file_get_contents('php://input'),1);
	$pdf = base64_decode($requestSUA['pdf']);

	$prefijo = substr(md5(uniqid(rand())),0,6);
	$rutaArchivo = '/usr/local/zend/var/apps/http/__default__/0/api/0.1_11/imagepro/apiSUA/';
	$archivo = $rutaArchivo.$prefijo.'_requestSUA.pdf';

	$ar=fopen($archivo,"a") or die("Problemas en la creacion");
  	fputs($ar,$pdf);
  	fclose($ar);

  	// Contabilizamos la paginas que contiene el PDF OK
	$num = shell_exec("identify -format %n $archivo");

	shell_exec("convert -density 600 \"".$archivo."[".($num-1)."]\" -depth 30 ".$rutaArchivo.$prefijo."_requestSUA.jpg");

	shell_exec('tesseract "/usr/local/zend/var/apps/http/__default__/0/api/0.1_11/imagepro/apiSUA/'.$prefijo.'_requestSUA.jpg" "/usr/local/zend/var/apps/http/__default__/0/api/0.1_11/imagepro/apiSUA/'.$prefijo.'_requestSUA"');

	$hojas[] = file_get_contents('/usr/local/zend/var/apps/http/__default__/0/api/0.1_11/imagepro/apiSUA/'.$prefijo.'_requestSUA.txt');
	
	unlink($rutaArchivo.$prefijo.'_requestSUA.pdf');
	unlink($rutaArchivo.$prefijo.'_requestSUA.jpg');
	unlink($rutaArchivo.$prefijo.'_requestSUA.txt');	
	
	ob_clean(0);
	echo json_encode($hojas);
?>