<?php 

namespace App\Controllers;

// use App\Models\{Job, Project};

class CargasController extends BaseController {

	function findInString($regex, $string, $findReverse = false) {
   		preg_match($regex, $string, $matches);
    	return $this->getResultsOfMatch($matches, $findReverse);
	}

	function getResultsOfMatch($matches, $findReverse) {
	    $result = [];
	    if($matches[0]){
			if($findReverse) {
				$splitedResults = array_values(array_filter(explode('-', $matches[0])));

				$label = 'Retenciones';
				$value = $splitedResults[0];
			} else {
				$label = "Total";
				$value = trim($matches[0]);
			}

	        $result = [
	            'label' => trim($label),
	            'value' => trim($value)
	        ];
	    }
	    return $result;
	}


	private function count_pages($pdfname) {
		$pdftext = file_get_contents($pdfname);
		$num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
		return $num;
	}

	private function curlExec($url, $stringJson) {
		$init = curl_init();
		curl_setopt($init, CURLOPT_URL, $url);
		curl_setopt($init, CURLOPT_CRLF, $url);
		curl_setopt($init, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt($init, CURLOPT_BUFFERSIZE, 16000);
		curl_setopt($init, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($init, CURLOPT_POST, 1);
		curl_setopt($init, CURLOPT_POSTFIELDS, $stringJson);
		curl_setopt($init, CURLOPT_HTTPHEADER, ['Content-Type:  application/json']);
		curl_setopt($init, CURLOPT_ENCODING, "UTF-8");
		curl_setopt($init, CURLOPT_TIMEOUT, 300); //timeout in seconds
		return curl_exec($init);
	}

	private function importSua($destino, $nameF){
		echo $_FILES['pdf']['tmp_name'];

		$rutaDoc = $destino;
		$extraccion = base64_encode(file_get_contents($rutaDoc));
		$json = ['pdf' => $extraccion];	

		

		$apiOcr = $this->curlExec('http://54.165.25.115/api/imagepro/pdfSUA.php', json_encode($json));


		$stringResponse = implode('', json_decode($apiOcr));

	
		



		$replaceCharacters   = array("\r\n", "\n", "\r", '"', "[", "]");
		$parsedString = str_replace($replaceCharacters, '-', $stringResponse);

		$regexTotalAPagar = "/(?<=Total a pagar:|Tolal a pagan)\S*(\s\b[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)?\b|\.[0-9]+\b)/";
		$totalAPagar = $this->findInString($regexTotalAPagar, $parsedString);

		$regexCuota = "/(\b[0-9]{1,3}(,[0-9]{3})*(\.[0-9]+)?\b|\.[0-9]+\b)-(,|_|\s)(.*?)(Tolal d9 D|Total de D|Total de d)/";

		echo "<br> $parsedString<br>";
		
		$retencionesSalarios = $this->findInString($regexCuota, $parsedString, true);

		var_dump($retencionesSalarios);
	

		$total = str_replace(",","", $totalAPagar['value']);
		$cuotaObrera = str_replace(",","", $retencionesSalarios['value']);
		$cuotaPatronal = floatval($total) - floatval($cuotaObrera);

		echo "<br> Total: $total<br>";
		echo "Cuota Obrera: $cuotaObrera<br>";
		echo "Cuota patronal: $cuotaPatronal<br>";
		die();


		if($total && $cuotaObrera) {
			echo "INSERT INTO validacionxml.importesSubcontratacion (PK_validacion, DAIsr, DAIva, suaPatronal, suaObrera, suaSubtotal, archivoSua) VALUES ($PK_validacion, 0, 0, $cuotaPatronal, $cuotaObrera, $total, '$nameF')";
			mysqli_query($conexion,"INSERT INTO validacionxml.importesSubcontratacion (PK_validacion, DAIsr, DAIva, suaPatronal, suaObrera, suaSubtotal, archivoSua) VALUES ($PK_validacion, 0, 0, $cuotaPatronal, $cuotaObrera, $total, '$nameF')");
		} else{
			echo "No se pudieron encontrar los valores en el archivo";
		}
	}


	public function cargarPDF() {

		// var_dump($_FILES['pdf']['name']);
		// die();
		
		$directorio = '../files/';

		if(isset($_FILES['pdf'])) {
			$tamano = $_FILES['pdf']['size'];
			$tipo = $_FILES['pdf']['type'];
			$archivo = $_FILES['pdf']['name'];
			$prefijo = substr(md5(uniqid(rand())),0,6);

			$nameF = $prefijo."_".$_FILES['pdf']['name'];
			$destino =  $directorio.$nameF;


			if(move_uploaded_file($_FILES['pdf']['tmp_name'], $destino)) {
				echo "Se movio el archivo a la carpeta files";
				$this->importSua($destino, $nameF);
			} else {
				echo "No se pudo mover el archivo";
			}
		}


	}
}